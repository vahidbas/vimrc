#!/bin/bash

if [ ! -f ~/.vim/autoload/plug.vim ]; then
  echo "vim-plug not found!"
  echo "Installing vim-plug..."
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

# download clang-format script
if [ ! -f ~/.vim/clang-format.py ]; then
  echo "clang-format.py not found!"
  echo "Downloading clang-format.py..."
  curl -fLo ~/.vim/clang-format.py --create-dirs https://llvm.org/svn/llvm-project/cfe/trunk/tools/clang-format/clang-format.py
fi

echo "Installing vimrc..."
cp vimrc ~/.vimrc

echo "Installing plugins..."
printf "Installing Plugins...\nRestart vim after finish. " | vim -c PlugUpdate -R -
