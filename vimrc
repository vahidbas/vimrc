" VIM Configuration File
" Description: Optimized for C/C++ development, but useful also for other things.
"

" set UTF-8 encoding
set enc=utf-8
set fenc=utf-8
set termencoding=utf-8
" disable vi compatibility (emulation of old bugs)
set nocompatible
" use indentation of previous line
set autoindent
" use intelligent indentation for C
set smartindent
" configure tabwidth and insert spaces instead of tabs
set tabstop=2        " tab width is 4 spaces
set shiftwidth=2     " indent also with 4 spaces
set expandtab        " expand tabs to spaces
" wrap lines 80
set textwidth=80
" turn syntax highlighting on
set t_Co=256
syntax on
" turn line numbers on
set number
" highlight matching braces
set showmatch
" intelligent comments C++
set comments=sl:/*,mb:\ *,elx:\ */
set formatoptions+=r
" spall check
set spell spelllang=en_us
" Enhanced keyboard mappings
"
" in normal mode F2 will save the file
nmap <F2> :w<CR>
" in insert mode F2 will exit insert, save, enters insert again
imap <F2> <ESC>:w<CR>i

" integrating clang-format
nmap <C-I> :pyf ~/.vim/clang-format.py<cr>
imap <C-I> <c-o>:pyf ~/.vim/clang-format.py<cr>

" easy window switch by Alt+arrows
nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

" vim-plug plugins list
call plug#begin('~/.vim/plugged')
" color scheme
Plug 'nanotech/jellybeans.vim'
" statusline enhancement
Plug 'vim-airline/vim-airline'
" latex
Plug 'lervag/vimtex',  { 'for': 'tex' }
" python
Plug 'klen/python-mode', { 'for': ['python', 'pyrex'] }
" json
Plug 'elzr/vim-json', { 'for': 'json' }
" html, css
Plug 'mattn/emmet-vim', { 'for': ['html', 'css' ] }
" file explorer and git tags
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'Xuyuanp/nerdtree-git-plugin', { 'on': 'NERDTreeToggle' }
call plug#end()

" set syntax coloring scheme
colorscheme jellybeans

" vimtex configuration
" viewer
let g:vimtex_view_method = 'mupdf'

" nerdtree configuration
" open nerdtree
nmap <C-n> :NERDTreeToggle<CR>

" clang-format
let g:clang_format_path='clang-format-3.7'
